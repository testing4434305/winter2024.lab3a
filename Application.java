public class Application{
	public static void main(String[] args){
	Student secondYear = new Student();
	secondYear.id = 2222222;
	secondYear.rScore = 22.2;
	secondYear.lastname = "Smith";
	secondYear.laptop = false;
	
	Student lastYear = new Student();
	lastYear.id = 4444444;
	lastYear.rScore = 35.4;
	lastYear.lastname = "Jones";
	lastYear.laptop = true;
	
	//Pinting for verification
	/*System.out.println(lastYear.id);
	 *System.out.println(secondYear.rScore);
	 *System.out.println(lastYear.lastname);
	 *System.out.println(secondYear.laptop);
	*/
	
	
	//secondYear.grades();
	Student[] section4 = new Student[3];
	section4[0] = secondYear;
	section4[1] = lastYear;
	
	section4[2] = new Student();
	
	//Third Student added
	section4[2].id = 3333333;
	section4[2].lastname = "Brown";
	section4[2].laptop = true;
	section4[2].rScore = 29.1;
	
	
	System.out.println(section4[2].id);
	System.out.println(section4[2].lastname);
	System.out.println(section4[2].laptop);
	System.out.println(section4[2].rScore);
	}
}